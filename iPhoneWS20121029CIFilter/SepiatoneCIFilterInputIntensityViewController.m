//
//  SepiatoneCIFilterInputIntensityViewController.m
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/08.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import "SepiatoneCIFilterInputIntensityViewController.h"

@interface SepiatoneCIFilterInputIntensityViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *originalImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterImageView;
@property (nonatomic, weak) IBOutlet UISlider *slider;

@end

@implementation SepiatoneCIFilterInputIntensityViewController

#pragma mark - applyFilter
- (void)applyFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"];
    [filter setDefaults];
    [filter setValue:ciImage forKey:@"inputImage"];
    [filter setValue:[NSNumber numberWithFloat: self.slider.value] forKey:@"inputIntensity"];

    CIImage *outputImage = filter.outputImage;

    CIContext *context = [CIContext contextWithOptions: nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterImageView.image = resultUIImage;
}

#pragma mark - UISlider Action method
- (IBAction)sliderValueChanged:(UISlider *)sender
{
    [self applyFilter];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self applyFilter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
