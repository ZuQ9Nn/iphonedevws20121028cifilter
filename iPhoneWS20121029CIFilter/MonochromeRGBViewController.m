//
//  MonochromeRGBViewController.m
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/07.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import "MonochromeRGBViewController.h"

@interface MonochromeRGBViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *originalImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterRedImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterBlueImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterGreenImageView;

@end

@implementation MonochromeRGBViewController

#pragma mark - applyFilter
- (void)applyFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterImageView.image = resultUIImage;
}

- (void)applyFilterRed
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];

    // inputColor Red = 1.0
    [filter setValue:[CIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f]
              forKey: @"inputColor"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterRedImageView.image = resultUIImage;
}

- (void)applyFilterGreen
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];

    // inputColor Green = 1.0
    [filter setValue:[CIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:1.0f]
              forKey: @"inputColor"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterGreenImageView.image = resultUIImage;
}

- (void)applyFilterBlue
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];

    // inputColor Blue = 1.0
    [filter setValue:[CIColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:1.0f]
              forKey: @"inputColor"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterBlueImageView.image = resultUIImage;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self applyFilter];
    [self applyFilterRed];
    [self applyFilterGreen];
    [self applyFilterBlue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
