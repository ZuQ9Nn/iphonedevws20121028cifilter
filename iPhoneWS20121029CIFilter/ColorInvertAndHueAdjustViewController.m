//
//  ColorInvertAndHueAdjustViewController.m
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/08.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import "ColorInvertAndHueAdjustViewController.h"

@interface ColorInvertAndHueAdjustViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *originalImageView;
@property (nonatomic, weak) IBOutlet UIImageView *colorInvertFilterImageView;
@property (nonatomic, weak) IBOutlet UIImageView *hueAdjustFilterImageView;
@property (nonatomic, weak) IBOutlet UIImageView *colorInvertAndHueAdjustFilterImageView;

@end

@implementation ColorInvertAndHueAdjustViewController

#pragma mark - applyFilter
- (void)applyColorInvertFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIColorInvert"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.colorInvertFilterImageView.image = resultUIImage;
}

- (void)applyHueAdjustFilterFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CIHueAdjust"];
    [filter setDefaults];
    [filter setValue:ciImage forKey: @"inputImage"];
    [filter setValue:[NSNumber numberWithFloat:1.62] forKey:@"inputAngle"];

    CIImage *outputImage = [filter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.hueAdjustFilterImageView.image = resultUIImage;
}

- (void)applyColorInvertAndHueAdjustFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *colorInvertfilter = [CIFilter filterWithName:@"CIColorInvert"];
    [colorInvertfilter setDefaults];
    [colorInvertfilter setValue:ciImage forKey: @"inputImage"];

    CIFilter *hueAdjustFilter = [CIFilter filterWithName:@"CIHueAdjust"];
    [hueAdjustFilter setDefaults];
    [hueAdjustFilter setValue:[colorInvertfilter valueForKey:@"outputImage"] forKey:@"inputImage"];
    [hueAdjustFilter setValue:[NSNumber numberWithFloat:1.62] forKey:@"inputAngle"];

    CIImage *outputImage = [hueAdjustFilter valueForKey:@"outputImage"];

    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.colorInvertAndHueAdjustFilterImageView.image = resultUIImage;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self applyColorInvertFilter];
    [self applyHueAdjustFilterFilter];
    [self applyColorInvertAndHueAdjustFilter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
