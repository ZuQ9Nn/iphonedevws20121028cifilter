//
//  AppDelegate.h
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/07.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
