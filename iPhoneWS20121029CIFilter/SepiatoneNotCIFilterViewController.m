//
//  SepiatoneNotCIFilterViewController.m
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/07.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import "SepiatoneNotCIFilterViewController.h"

@interface SepiatoneNotCIFilterViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *originalImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterImageView;

@end

@implementation SepiatoneNotCIFilterViewController

#pragma mark - applyFilter
- (void)applyFilter
{
    CGImageRef cgImageRef = self.originalImageView.image.CGImage;

    size_t width  = CGImageGetWidth(cgImageRef);
    size_t height = CGImageGetHeight(cgImageRef);
    
    size_t bitsPerComponent = CGImageGetBitsPerComponent(cgImageRef);
    size_t bitsPerPixel = CGImageGetBitsPerPixel(cgImageRef);
    size_t bytesPerRow = CGImageGetBytesPerRow(cgImageRef);

    CGColorSpaceRef colorSpace = CGImageGetColorSpace(cgImageRef);
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(cgImageRef);
    
    bool shouldInterpolate = CGImageGetShouldInterpolate(cgImageRef);

    CGColorRenderingIntent intent = CGImageGetRenderingIntent(cgImageRef);
    CGDataProviderRef dataProvider = CGImageGetDataProvider(cgImageRef);

    CFDataRef data = CGDataProviderCopyData(dataProvider);
    UInt8 *buffer = (UInt8*)CFDataGetBytePtr(data);

    NSUInteger  x, y;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            
            UInt8 *tmp = buffer + y * bytesPerRow + x * 4;

            UInt8 red = *(tmp + 0);
            UInt8 green = *(tmp + 1);
            UInt8 blue = *(tmp + 2);
            
            UInt8 brightness = (107 * red + 74 * green + 43 * blue) / 256;

            *(tmp + 0) = brightness;
            *(tmp + 1) = brightness * 0.7;
            *(tmp + 2) = brightness * 0.4;
        }
    }

    CFDataRef effectedData = CFDataCreate(NULL, buffer, CFDataGetLength(data));
    CGDataProviderRef effectedDataProvider = CGDataProviderCreateWithCFData(effectedData);

    CGImageRef effectedCgImage = CGImageCreate(width, height,
                                    bitsPerComponent, bitsPerPixel,
                                    bytesPerRow, colorSpace,
                                    bitmapInfo, effectedDataProvider,
                                    NULL, shouldInterpolate, intent);
    
    self.filterImageView.image = [[UIImage alloc] initWithCGImage:effectedCgImage];

    CGImageRelease(effectedCgImage);
    CFRelease(effectedDataProvider);
    CFRelease(effectedData);
    CFRelease(data);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self applyFilter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
