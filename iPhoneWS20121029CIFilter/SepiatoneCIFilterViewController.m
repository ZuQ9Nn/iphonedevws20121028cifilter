//
//  SepiatoneCIFilterViewController.m
//  iPhoneWS20121029CIFilter
//
//  Created by ZuQ9Nn on 2012/10/07.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import "SepiatoneCIFilterViewController.h"
#import <CoreImage/CoreImage.h>

@interface SepiatoneCIFilterViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *originalImageView;
@property (nonatomic, weak) IBOutlet UIImageView *filterImageView;

@end

@implementation SepiatoneCIFilterViewController

#pragma mark - applyFilter
- (void)applyFilter
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.originalImageView.image];

    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"];
    [filter setDefaults];
    [filter setValue:ciImage forKey:@"inputImage"];
    [filter setValue:[NSNumber numberWithFloat: 0.9f] forKey:@"inputIntensity"];

    CIImage *outputImage = filter.outputImage;

    CIContext *context = [CIContext contextWithOptions: nil];
    CGImageRef cgImage = [context createCGImage: outputImage fromRect: outputImage.extent];
    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
    CGImageRelease(cgImage);

    self.filterImageView.image = resultUIImage;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self applyFilter];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
