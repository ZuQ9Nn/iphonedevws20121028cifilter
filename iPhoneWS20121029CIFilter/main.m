//
//  main.m
//  iPhoneWS20121029CIFilter
//
//  Created by kazuki_tanaka on 2012/10/07.
//  Copyright (c) 2012年 ZuQ9Nn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
